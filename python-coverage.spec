Name:           python-coverage
Summary:        Code coverage testing module for Python
Version:        3.2
Release:        3
License:        BSD
Group:          System Environment/Libraries
URL:            http://nedbatchelder.com/code/modules/coverage.html
Source0:        http://pypi.python.org/packages/source/c/coverage/coverage-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  python-setuptools, python-devel
%(rpm-pyconfig --ifany-3 --eval "
BuildRequires:  /usr/bin/2to3
BuildRequires:  python3-setuptools, python3-devel
")
# FIXME: enumerate the build requirements?

%global SHARED_DESC \
Coverage.py is a Python module that measures code coverage during Python\
execution. It uses the code analysis tools and tracing hooks provided in the\
Python standard library to determine which lines are executable, and which\
have been executed.\
%{nil}


%description
%SHARED_DESC

# Define the metadata for each built package:
%(rpm-pyconfig --foreach --eval "
%package -n @confpkg-coverage
Summary: Code coverage testing module for @confpkg
Group: System Environment/Libraries
Requires: @confpkg-setuptools
%description -n @confpkg-coverage
%SHARED_DESC

@confdescline
")

%prep
%setup -q -n coverage-%{version}
chmod -x README.txt
sed -i 's/\r//g' README.txt
chmod -x coverage.egg-info/*.txt

# Generate pristine Python 2 code
cp -a . ../pristine

# Generate pristine Python 3 code if needed:
rpm-pyconfig --ifany-3 && (
   rm -rf %{py3dir}
   cp -a . %{py3dir}
   pushd %{py3dir}
   2to3 --nobackups --write .
   popd
)

# Each config gets its own source tree.  This is possibly paranoid
rpm-pyconfig --foreach-2 --exe \
   "cp -a ../pristine @confsrcdir"

rpm-pyconfig --foreach-3 --exe \
   "cp -a %{py3dir} @confsrcdir"

%build
%(rpm-pyconfig --foreach --eval "
    pushd @confsrcdir
    @confbin setup.py build
    popd
")

%install
rm -rf %{buildroot}

%(rpm-pyconfig --foreach --eval "
    pushd @confsrcdir
    @confbin setup.py install -O1 --skip-build --root %{buildroot}
    mv %{buildroot}/%{_bindir}/coverage %{buildroot}/%{_bindir}/@confbin-coverage
    popd
")

%clean
rm -rf %{buildroot}

# %files for the various subpackages:
%(rpm-pyconfig --foreach --eval "
%files -n @confpkg-coverage
%defattr(-,root,root,-)
%doc README.txt
%{_bindir}/@confbin-coverage
@conf_sitearch/coverage/
@conf_sitearch/coverage*.egg-info/
")


%changelog
* Fri Feb  5 2010 David Malcolm <dmalcolm@redhat.com> - 3.2-3
- add python 3 subpackage (#536948)

* Sun Jan 17 2010 Luke Macken <lmacken@redhat.com> - 3.2-2
- Require python-setuptools (#556290)

* Wed Dec  9 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 3.2-1
- update to 3.2

* Fri Oct 16 2009 Luke Macken <lmacken@redhat.com> - 3.1-1
- Update to 3.1

* Wed Aug 10 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 3.0.1-1
- update to 3.0.1

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.85-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri May 15 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 2.85-2
- fix install invocation

* Wed May 6 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 2.85-1
- Initial package for Fedora
